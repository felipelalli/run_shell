extern crate run_shell;
use run_shell::*;

fn main() {
    // Run command by cmd! macro
    cmd!("echo Hello rust shell!").run().unwrap();

    // Contain white space or non-alphabetical characters
    cmd!("echo \"%$#\"").run().unwrap();

    // Pass an argument
    let name = "shell";
    cmd!("echo Hello rust {}!", name).run().unwrap();

    // Extract environment variable
    cmd!("echo HOME is $HOME").run().unwrap();

    fn shell_function() -> ShellResult {
        cmd!("echo Command A").run()?;
        cmd!("echo Command B").run()?;
        run_shell::ok()
    }
    shell_function().expect("shell_function failed");

    assert_eq!(cmd!("echo OK").stdout_utf8().unwrap(), "OK\n");

    let child = cmd!("sleep 2").spawn().unwrap();
    child.wait().unwrap();

    // Signal
    let child = cmd!("sleep 2").spawn().unwrap();
    let result = child.wait();
    assert!(result.status().is_ok(), "Still able to obtain status");

    let handle = run_shell::spawn(|| -> ShellResult { cmd!("sleep 3").run() });
    let result = handle.join().unwrap();
    assert!(result.status().is_ok(), "Still able to obtain status");

    use std::io::Read;
    use std::process::Stdio;

    // Access std::process::Command.
    let mut shell_command = cmd!("echo OK");
    {
        let command = &mut shell_command.command;
        command.stdout(Stdio::piped());
    }

    // Access std::process::Child.
    let shell_child = shell_command.spawn().unwrap();
    {
        let mut lock = shell_child.0.write().unwrap();
        let child = &mut lock.as_mut().unwrap().child;
        let mut str = String::new();
        child
            .stdout
            .as_mut()
            .unwrap()
            .read_to_string(&mut str)
            .unwrap();
    }
    shell_child.wait().unwrap();
}
